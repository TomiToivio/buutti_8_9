//Write a command line unit converter that converts between volume units. Your converter should accept at least units deciliter, liter, ounce, cup, and pint.
//The program takes three parameters: amount, source unit, and the target unit. For example 
//convert 6 dl oz --> 20
//Write tests for your converter. Include at least five tests!

//const converterArgs = process.argv.slice(2);
//const amount = Number(converterArgs[0]);
//const sourceUnit = String(converterArgs[1]);
//const targetUnit = String(converterArgs[2]);
export default function convert(amount,sourceUnit,targetUnit) {
    if(typeof amount !== "number") {
        throw new Error("Incorrect amount");
    }
    console.log(sourceUnit);
    let liters = 0;
    switch (String(sourceUnit))
    {
    case "dl":
        liters = 0.1 * amount;
        break;
    case "li":
        liters = amount;
        break;
    case "oz":
        liters = amount / 33.8140227;
        break;
    case "cup":
        liters = amount / 4.22675284;
        break;
    case "pint":
        liters = amount / 2.11337642;
        break;
    default:
        throw new Error("No such source unit");
    }

    let result = 0;
    switch (String(targetUnit))
    {
    case "dl":
        result = 10 * liters;
        break;
    case "li":
        result = 1 * liters;
        break;
    case "oz":
        result = 33.8140227 * liters;
        break;
    case "cup":
        result = 4.22675284 * liters;
        break;
    case "pint":
        result = 2.11337642 * liters;
        break;
    default:
        throw new Error("No such target unit");
    }
    return result;
}
//console.log(convert(amount,sourceUnit,targetUnit));