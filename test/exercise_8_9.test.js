import convert from '../src/exercise_8_9.js';

test('Deciliter test', () => {
    expect(convert(3,"li","dl")).toBe(30);
});
test('Liter test', () => {
    expect(convert(10,"dl","li")).toBe(1);
});
test('Pint test', () => {
    expect(convert(10,"pint","li")).toBe(4.7317647274592005);
});
test('Ounce test', () => {
    expect(convert(100,"oz","li")).toBe(2.9573529564111873);
});
test('Cup test', () => {
    expect(convert(9,"cup","li")).toBe(2.12929412735664);
});